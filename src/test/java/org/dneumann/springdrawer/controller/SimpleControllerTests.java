package org.dneumann.springdrawer.controller;

import org.dneumann.springdrawer.controller.Controller;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
public class SimpleControllerTests {

    @Inject
    Controller controller;

    @BeforeEach
    public void initializeCanvas() {
        controller.createCanvas(20, 15);
    }

    @Test
    public void checkIfCanvasIsCreated() {
        assertEquals(20, controller.getCanvasWidth());
        assertEquals(15, controller.getCanvasHeight());
    }

    @Test
    public void drawHorizontalLineResultTrueTest() {
        assertTrue(controller.drawLine(1,1,5,1));
    }

    @Test
    public void drawVerticalLineResultTrueTest() {
        assertTrue(controller.drawLine(1,1,1,5));
    }

    @Test
    public void drawDiagonalLineResultFalseTest() {
        assertFalse(controller.drawLine(1,1,5,5));
    }

    @Test
    public void drawLineWrongCoordinateXResultFalseTest() {
        assertFalse(controller.drawLine(-1,1,5,5));
    }

    @Test
    public void drawLineWrongCoordinateYResultFalseTest() {
        assertFalse(controller.drawLine(1,-1,5,5));
    }

    @Test
    public void drawRectangleResultsTrue() {
        assertTrue(controller.drawRectangle(1,1,5,5));
    }

    @Test
    public void drawRectangleCoordinatesSwappedResultsTrue() {
        assertTrue(controller.drawRectangle(5, 5, 1, 1));
    }

    @Test
    public void drawRectangleWrongCoordinateXResultsFalse() {
        assertFalse(controller.drawRectangle(-1, 1, 5, 5));
    }

    @Test
    public void drawRectangleWrongCoordinateYResultsFalse() {
        assertFalse(controller.drawRectangle(1, 1, 5, -5));
    }
}

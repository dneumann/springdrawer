package org.dneumann.springdrawer.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest
public class SimpleCanvasTests {

    @Inject
    Canvas canvas;

    @BeforeEach
    public void createCanvas() {
        canvas.createCanvas(20, 15);
    }

    @Test
    public void checkCanvasSize() {
        assertEquals(20,canvas.getCanvasWidth());
        assertEquals(15, canvas.getCanvasHeight());
    }

    @Test
    public void testDrawVerticalLine() {
        canvas.drawLine(1, 1, 1, 5);
        for (int y = 1; y <=5; y++) {
            assertEquals('.', canvas.getField(1,y));
        }
        assertEquals(' ', canvas.getField(1,6));
    }

    @Test
    public void testDrawHorizontalLine() {
        canvas.drawLine(1, 1, 5, 1);
        for (int x = 1; x <=5; x++) {
            assertEquals('.', canvas.getField(x,1));
        }
        assertEquals(' ', canvas.getField(6,1));
    }

    @Test
    public void testDrawDiagonalLine() {
        canvas.drawLine(1, 1, 5, 5);
        for (int x = 1; x <= 5; x++) {
            for (int y = 1; y <= 5; y++) {
                assertEquals(' ', canvas.getField(x, y));
            }
        }
    }

    @Test
    public void testDrawRectangle() {
        canvas.drawRectangle(1, 1, 5, 5);
        for (int x = 1; x <= 5; x++) {
            assertEquals('.', canvas.getField(x, 1));
            assertEquals('.', canvas.getField(x, 5));
        }
        for (int y = 1; y <= 5; y++) {
            assertEquals('.', canvas.getField(1, y));
            assertEquals('.', canvas.getField(5, y));
        }
    }

    @Test
    public void testfillRectangle() {
        canvas.drawRectangle(1, 1, 5, 5);
        canvas.fillWithColour(3,3,'r');
        for (int x = 1; x <= 5; x++) {
            assertEquals('.', canvas.getField(x, 1));
            assertEquals('.', canvas.getField(x, 5));
        }
        for (int y = 1; y <= 5; y++) {
            assertEquals('.', canvas.getField(1, y));
            assertEquals('.', canvas.getField(5, y));
        }
        for (int x = 2; x <= 4; x++) {
            for (int y = 2; y <= 4; y++) {
                assertEquals('r', canvas.getField(x, y));
            }
        }
    }
}

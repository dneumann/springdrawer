package org.dneumann.springdrawer;

import org.dneumann.springdrawer.controller.Controller;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Scanner;

@Profile("test")
@Component
public class SimpleDrawerRunner {

    @Inject
    Controller controller;

    public void run(String... args) throws Exception {
    }
}

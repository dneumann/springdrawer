package org.dneumann.springdrawer;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class SpringdrawerApplicationTests {

	@Test
	void contextLoads() {
	}

}

package org.dneumann.springdrawer.View;

import org.dneumann.springdrawer.model.Canvas;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class SimpleView implements View{

    @Inject
    Canvas canvas;

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }

    public void printCanvas() {
        if (canvas != null) {
            canvas.print();
        }
    }
}

package org.dneumann.springdrawer.View;

import org.dneumann.springdrawer.model.Canvas;

public interface View {

    void setCanvas(Canvas canvas);

    void printCanvas();
}

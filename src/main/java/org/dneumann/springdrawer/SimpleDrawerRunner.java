package org.dneumann.springdrawer;

import org.dneumann.springdrawer.controller.Controller;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Scanner;

@Profile("!test")
@Component
public class SimpleDrawerRunner {

    @Inject
    Controller controller;

    public void run(String... args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        boolean finish = false;
        while (!finish) {
            controller.printCanvas();
            System.out.print("$");
            String[] command = scanner.nextLine().split(" ");
            switch (command[0]) {
                case "C":
                case "c":
                    controller.createCanvas(Integer.valueOf(command[1]), Integer.valueOf(command[2]));
                    break;
                case "L":
                case "l":
                    controller.drawLine(Integer.valueOf(command[1]), Integer.valueOf(command[2]), Integer.valueOf(command[3]), Integer.valueOf(command[4]));
                    break;
                case "R":
                case "r":
                    controller.drawRectangle(Integer.valueOf(command[1]), Integer.valueOf(command[2]), Integer.valueOf(command[3]), Integer.valueOf(command[4]));
                    break;
                case "F":
                case "f":
                    controller.fillWithColour(Integer.valueOf(command[1]), Integer.valueOf(command[2]), command[3].charAt(0));
                    break;
                case "q":
                    controller.exit();
                    finish = true;
                    break;
                default:
                    controller.invalidCommand();
            }
        }
        System.exit(0);
    }
}

package org.dneumann.springdrawer.model;

import org.springframework.stereotype.Component;

@Component
public class SimpleCanvas  implements Canvas {

    private char[][] canvas;

    public SimpleCanvas() {
        createCanvas(1,1);
    }

    private void initializeCanvas() {
        if (canvas == null) return;
        for (int x = 0; x < canvas.length; x++) {
            for (int y = 0; y < canvas[0].length; y++) {
                canvas[x][y] = ' ';
            }
        }
    }

    @Override
    public void createCanvas(int width, int height) {
        canvas = new char[width][height];
        initializeCanvas();
    }

    public char getField(int x, int y) {
        return canvas[x - 1][y - 1];
    }

    public void setField(int x, int y, char character) {
        canvas[x - 1][canvas[0].length - y] = character;
    }

    @Override
    public void drawLine(int startingX, int startingY, int endingX, int endingY) {
        if ((startingX == endingX) || (startingY == endingY)) {
            for (int x = startingX - 1; x < endingX; x++) {
                for (int y = startingY - 1; y < endingY; y++) {
                    canvas[x][y] = '.';
                }
            }
        }
    }

    @Override
    public void drawRectangle(int startingX, int startingY, int endingX, int endingY) {
        for (int x = startingX-1; x < endingX-1; x++) {
            canvas[x][startingY-1] = '.';
        }
        for (int x = startingX-1; x < endingX; x++) {
            canvas[x][endingY-1] = '.';
        }
        for (int y = startingY-1; y < endingY-1; y++) {
            canvas[startingX-1][y] = '.';
        }
        for (int y = startingY-1; y < endingY-1; y++) {
            canvas[endingX-1][y] = '.';
        }
    }

    @Override
    public void fillWithColour(int x, int y, char colour) {
        fillingWithColour(x, y, colour);
    }

    private void fillingWithColour(int x, int y, char colour) {
        if ((canvas[x][y] == '.') || (canvas[x][y] == colour) || (x - 1 < 0) || (x >= canvas.length) || (y - 1 < 0) || (y >= canvas[0].length)) {
            return;
        }
        canvas[x][y] = colour;
        fillingWithColour(x - 1, y, colour);
        fillingWithColour(x + 1, y, colour);
        fillingWithColour(x, y - 1, colour);
        fillingWithColour(x, y + 1, colour);
    }

    @Override
    public void print() {
        for (int y = canvas[0].length - 1; y >= 0; y--) {
            for (int x = 0; x < canvas.length; x++) {
                System.out.print(canvas[x][y] + " ");
            }
            System.out.println();
        }
    }

    @Override
    public int getCanvasWidth() {
        return canvas.length;
    }

    @Override
    public int getCanvasHeight() {
        return canvas[0].length;
    }
}
package org.dneumann.springdrawer.model;

public interface Canvas {

    void createCanvas(int width, int height);

    char getField(int x, int y);

    void setField(int x, int y, char character);

    void drawLine(int startingX, int startingY, int endingX, int endingY);

    void drawRectangle(int startingX, int startingY, int endingX, int endingY);

    void fillWithColour(int x, int y, char colour);

    void print();

    int getCanvasWidth();

    int getCanvasHeight();
}

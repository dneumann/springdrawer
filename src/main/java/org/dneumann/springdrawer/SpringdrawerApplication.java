package org.dneumann.springdrawer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.inject.Inject;

@SpringBootApplication
public class SpringdrawerApplication implements CommandLineRunner {

	@Inject
	SimpleDrawerRunner simpleDrawerRunner;


	public static void main(String[] args) {
		SpringApplication.run(SpringdrawerApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		simpleDrawerRunner.run(args);
	}



}

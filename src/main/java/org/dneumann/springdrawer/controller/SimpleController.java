package org.dneumann.springdrawer.controller;

import lombok.Data;
import org.dneumann.springdrawer.View.View;
import org.dneumann.springdrawer.model.Canvas;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
@Data
public class SimpleController implements Controller{
    @Inject
    Canvas canvas;
    @Inject
    View view;

    public void createCanvas(int width, int height){
        canvas.createCanvas(width,height);
        view.setCanvas(canvas);
    }

    @Override
    public boolean drawLine(int startingX, int startingY, int endingX, int endingY) {
        boolean result = false;
        if ((canvas != null) && ((startingX == endingX) || (startingY == endingY))) {
            if (startingX > endingX) {
                endingX += startingX;
                startingX = endingX - startingX;
                endingX -= startingX;
            }
            if (startingY > endingY) {
                endingY += startingY;
                startingY = endingY - startingY;
                endingY -= startingY;
            }
            canvas.drawLine(startingX, startingY, endingX, endingY);
            result = true;
        }
        return result;
    }

    @Override
    public boolean drawRectangle(int startingX, int startingY, int endingX, int endingY) {
        boolean result = false;
        if ((canvas != null) && (checkCoordinateConditions(startingX, startingY, endingX, endingY))) {
            if (startingX > endingX) {
                endingX += startingX;
                startingX = endingX - startingX;
                endingX -= startingX;
            }
            if (startingY > endingY) {
                endingY += startingY;
                startingY = endingY - startingY;
                endingY -= startingY;
            }
            canvas.drawRectangle(startingX, startingY, endingX, endingY);
            result = true;
        }
        return result;

    }

    private boolean checkCoordinateConditions(int startingX, int startingY, int endingX, int endingY) {
        boolean result = false;
        if ((startingX > 0) && (startingY > 0) && (endingX > 0) && (endingY > 0) &&
                (startingX <= canvas.getCanvasWidth()) && (endingX <= canvas.getCanvasWidth()) &&
                (startingY <= canvas.getCanvasHeight()) && (endingY <= canvas.getCanvasHeight())) {
            result = true;
        }
        return result;
    }

    @Override
    public void fillWithColour(int x, int y, char colour) {
        if (canvas != null) {
            canvas.fillWithColour(x, y, colour);
        }

    }

    @Override
    public void exit() {
        System.out.print("exiting...");
    }

    @Override
    public void invalidCommand() {
        System.out.println("Invalid command");
    }

    @Override
    public void printCanvas() {
        view.printCanvas();
    }

    @Override
    public int getCanvasWidth() {
        return canvas.getCanvasWidth();
    }

    @Override
    public int getCanvasHeight() {
        return canvas.getCanvasHeight();
    }
}

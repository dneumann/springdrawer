package org.dneumann.springdrawer.controller;

public class SystemOutController implements Controller{


    @Override
    public void createCanvas(int width, int height) {
        System.out.println(String.format("creating canvas %dx%d", width, height));
    }

    @Override
    public boolean drawLine(int startingX, int startingY, int endingX, int endingY) {
        System.out.println(String.format("Drawing line (%d,%d)-(%d,%d)", startingX, endingX, startingY, endingY));
        return false;
    }

    @Override
    public boolean drawRectangle(int startingX, int startingY, int endingX, int endingY) {
        System.out.println(String.format("Drawing rectangle (%d,%d)-(%d,%d)", startingX,  startingY, endingX, endingY));
        return true;
    }

    @Override
    public void fillWithColour(int x, int y, char colour) {
        System.out.println(String.format("Filling (%d,%d) with colour %s", x, y, colour));
    }

    @Override
    public void exit() {
        System.out.print("exiting...");
    }

    @Override
    public void invalidCommand() {
        System.out.println("Invalid command");
    }

    @Override
    public void printCanvas() {

    }

    @Override
    public int getCanvasWidth() {
        return 0;
    }

    @Override
    public int getCanvasHeight() {
        return 0;
    }
}

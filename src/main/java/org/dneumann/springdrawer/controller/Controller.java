package org.dneumann.springdrawer.controller;

public interface Controller {

    void createCanvas(int width, int height);

    boolean drawLine(int startingX, int startingY, int endingX, int endingY);

    boolean drawRectangle(int startingX, int startingY, int endingX, int endingY);

    void fillWithColour(int x, int y, char colour);

    void exit();

    void invalidCommand();

    void printCanvas();

    int getCanvasWidth();

    int getCanvasHeight();
}

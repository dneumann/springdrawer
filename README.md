# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains command line drawer application. For drawing use specific instructions:
create new canvas (w: width, h: heigh)

   C w h

- draw a line (only vertical and horizontal lines are supported)

   L x1 y1 x2 y2

- draw a rectangle

   R x1 y1 x2 y2

- fill the connected area with c colour from x, y coordinates

   F x y c

- quit

   Q
   
   
   Case insensitive

### How do I get set up? ###

 Building instructions:
 
 - run command mvn clean install
 
 Running instructions:
 
 - run command mvn spring-boot::run
 
### Contribution guidelines ###


### Who do I talk to? ###

### Assumptions ###
 
 - Canvas is treated as collection of pixels, so when drawing a line used pixel is represented as ".". Instead of filling it with colours, canvas if filled with letter as representation.
 
 - Coordinates (1,1) are located in lower-left corner
 
 - commands are case insensitive
